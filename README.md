### Description
This tool helps to make backups from postgres instances, encrypting and uploading them to s3 buckets.
### Stack used
* [gnupg](https://www.gnupg.org/)
* [psql](https://www.postgresql.org/docs/current/)
* [aws cli](https://aws.amazon.com/cli/)
### Environment variables
Listed in .env-example
### Restore from backup
In order to restore from backup first download from s3 bucket, decrypt the backup and upload it with the following commands:
```
$ gpg --decrypt backup.psql.gz.gpg --output backup.psql.gz 
$ gzip -d -f backup.psql.gz
$ pg_restore backup.psql --dbname=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:5432/Database_Name
```

