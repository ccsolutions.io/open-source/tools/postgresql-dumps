#!/bin/sh
#source .env

if [[ -z "$POSTGRES_USER" ]]; then
        echo "Must provide POSTGRES_USER in environment."
        exit 2
fi
if [[ -z "$POSTGRES_PASSWORD" ]]; then
        echo "Must provide POSTGRES_PASSWORD in environment."
        exit 2
fi
if [[ -z "$POSTGRES_HOST" ]]; then
        echo "Must provide POSTGRES_HOST in environment."
        exit 2
fi
if [[ -z "$POSTGRES_PORT" ]]; then
        echo "No port provided, using default postgres port 5432 instead"
        POSTGRES_PORT="5432"
        exit 2
fi
if [[ -z "$GPG_KEY" ]]; then
        echo "Must provide GPG_KEY in environment."
        exit 2
fi
if [[ -z "$GPG_RECIPIENT" ]]; then
        echo "Must provide GPG_RECIPIENT in environment."
        exit 2
fi
if [[ -z "$S3_PATH" ]]; then
        echo "Must provide S3_PATH in environment."
        exit 2
fi
if [[ -z "$AWS_ACCESS_KEY_ID" ]]; then
        echo "Must provide AWS_ACCESS_KEY_ID in environment."
        exit 2
fi
if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
        echo "Must provide AWS_SECRET_ACCESS_KEY in environment."
        exit 2
fi
if [[ -z "$S3_ENDPOINT" ]]; then
        echo "Must provide S3_ENDPOINT in environment."
        exit 2
fi
if [[ -z "$S3_PATH" ]]; then
        echo "Must provide S3_PATH in environment."
        exit 2
fi
if [[ -z "$HEALTHCHECK_ID" ]]; then
        echo "Must provide HEALTHCHECK_ID (healthchecks.io) in environment."
        exit 2
fi
echo "GPG_KEY import is starting"
echo "$GPG_KEY" | gpg --batch --import --verbose
echo "GPG_KEY import is done"
if ! [ $? = "0" ]; then
  echo "GPG_KEY import failed"
  curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID/fail
else
echo "GPG_KEY import successful"
curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID
dbs=$(psql --dbname=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT -t -A -c "select datname from pg_database where datistemplate = false")
echo Found Databases: ${dbs}
for db in ${dbs}; do
        if ! [ "$db" = "postgres" ] && ! [ "$db" = "template0" ] && ! [ "$db" = "template1" ]; then
                backup_name="$(date +%s)_$db.tar.gz"
                echo $backup_name
                pg_dump --dbname=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$db | gzip > $backup_name
                encrypted_name="$backup_name.gpg"
                gpg --always-trust -r $GPG_RECIPIENT -o $encrypted_name --encrypt "$backup_name"
                if ! [ $? = "0" ]; then
                  echo "GPG encryption failed. Upload will not occur."
                  curl --retry 5 https://hc-ping.com/$HEALTHCHECK_ID/fail
                fi
                aws s3 cp $encrypted_name $S3_PATH --endpoint-url "https://$S3_ENDPOINT"
        fi
done
fi
rm *.gz*